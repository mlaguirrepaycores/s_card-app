package com.mlaj.scard;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.mlaj.scard.Fragments.AboutFragment;
import com.mlaj.scard.Fragments.CameraFragment;
import com.mlaj.scard.Fragments.FavoritesFragment;
import com.mlaj.scard.Fragments.HomeFragment;
import com.mlaj.scard.Fragments.ProfileFragment;
import com.mlaj.scard.Fragments.SearchFragment;
import com.mlaj.scard.Interfaces.IFragments;

//Clase principal donde se cargan los fragmentos y esta incluido el navigationDrawer
//Se implementa el IFragments donde se agregan los OnFragmentInteractionListener de cada uno de los fragmentos.
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, IFragments {

    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@NonNull View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();

        //carga inicial del fragmento Home, para que sea el que inicia cuando se inicia la sesión
        fragment = new HomeFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, fragment).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }
        */
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Se manejan los elementos de la vista de navegacion.
        int id = item.getItemId();
        boolean fragmentSeleccionado = false;
        if (id == R.id.nav_home) {
            fragment = new HomeFragment();
            fragmentSeleccionado = true;

        } else if (id == R.id.nav_profile) {
            fragment = new ProfileFragment();
            fragmentSeleccionado = true;

        } else if (id == R.id.nav_favorites) {
            fragment = new FavoritesFragment();
            fragmentSeleccionado = true;

        } else if (id == R.id.nav_search) {
            fragment = new SearchFragment();
            fragmentSeleccionado = true;

        } else if (id == R.id.nav_camera) {
            fragment = new CameraFragment();
            fragmentSeleccionado = true;

        } else if (id == R.id.nav_mi_scard) {
            fragment = new AboutFragment();
            fragmentSeleccionado = true;

        } else if (id == R.id.nav_exit) {

        }
        if (fragmentSeleccionado) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, fragment).commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
