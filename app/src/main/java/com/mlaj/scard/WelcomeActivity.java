package com.mlaj.scard;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mlaj.scard.Models.User;
import com.mlaj.scard.Models.VolleySingleton;
import com.mlaj.scard.Utils.Preferences;
import com.mlaj.scard.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//Clase utilizada para mostrar al usuario qu ha finalizado el proceso de registro ademas de
//realizar el login automaticamente
public class WelcomeActivity extends AppCompatActivity {

    User user;
    MyProgressDialog dialog;
    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        //Se obtiene el User que fue enviado como paramentro desde QrReader
        user = (User) getIntent().getExtras().get("user");

        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    //Se ejecuta el metodo LoginWebService
                    LoginWebService();
                }
            }
        };
        timer.start();
    }

    //Web Service de Inicio de sesion
    private void LoginWebService() {
        //Inicio de Progress Dialog
        dialog = new MyProgressDialog(this);
        dialog.setMessage("Comprobando ...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.show();

        /*
         * URL donde se encuentra alojado el Servicio.
         * Se envian como parametros el numero de identificación, la contraseña, ambos ingresados por el usuario
         */

        String url = Utils.URL + "/wsJSONLogin.php?identifier=" + user.getId_user() + "&password=" + user.getPassword();

        //inicialización de JSON donde se pide un objeto a traves de un GET
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Se obtiene la respuesta del servicio
                JSONArray json = response.optJSONArray("client");
                JSONObject jsonObject = null;

                try {
                    jsonObject = json.getJSONObject(0);
                    if (jsonObject.getBoolean(Utils.STATUS)) {

                        //Si la respuesta es TRUE se asignan los valores a las Preferences
                        Preferences.setPreferenceS(WelcomeActivity.this, Utils.ID_USER, jsonObject.optString("id_user") + "");
                        Preferences.setPreferenceS(WelcomeActivity.this, Utils.NAME, jsonObject.optString("name"));
                        Preferences.setPreferenceS(WelcomeActivity.this, Utils.PHONE, jsonObject.optInt("phone") + "");
                        Preferences.setPreferenceS(WelcomeActivity.this, Utils.EMAIL, jsonObject.optString("email"));
                        Preferences.setPreferenceS(WelcomeActivity.this, Utils.PASSWORD, jsonObject.optString("password"));
                        Preferences.setPreferenceS(WelcomeActivity.this, Utils.SOCIAL_NETWORK, jsonObject.optInt("social_network") + "");
                        Preferences.setPreferenceS(WelcomeActivity.this, Utils.PAY_TYPE, jsonObject.optInt("pay_type") + "");
                        Preferences.setPreferenceS(WelcomeActivity.this, Utils.CODE, jsonObject.optInt("code") + "");
                        Preferences.setPreferenceS(WelcomeActivity.this, Utils.LOADHOME, "0");

                        //Se accede a la actividad MainActivity.
                        startActivity(new Intent(WelcomeActivity.this, MainActivity.class));

                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    } else {
                        //Si la respuesta es FALSE se envia un mensaje de error.
                        Toast.makeText(getApplicationContext(), "Error en las credenciales", Toast.LENGTH_SHORT).show();
                    }
                    //Se finaliza el progress dialog
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //En caso de no poder acceder al servicio se muestra uno de los siguientes mensajes de error.
                dialog.dismiss();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getApplicationContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(), R.string.authFailureError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(), R.string.serverError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getApplicationContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(), R.string.parseError, Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Se agrega la petición
        VolleySingleton.getIntanciaVolley(this).addToRequestQueue(jsonObjectRequest);
    }
}
