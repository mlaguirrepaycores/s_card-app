package com.mlaj.scard.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by paycores.com.
 * @author Leandro Aguirre
 *
 * Permite crear los SharedPrefences de la aplicacion
 * y los metodos correspondientes para agregar y obtener cada dato guardado.
 */
public class Preferences {

    private static final String PREFERENCE_N = "PrefWalletN"; //Prefencia que se usa para datos de la wallet
    private static final String PREFERENCE_R = "PrefWalletR"; //Prefencia que se usa para datos del registro
    private static final String JSON_DATA = "Wallet";
    public static final int TYPE_STANDARD = 0;
    public static final int TYPE_REGISTER = 1;

    /**
     * Guarda el valor de una preferencia de cadena
     * @param context - en donde se ejecuta
     * @param type - Tipo de preferencia que se va a usar
     * @param key - identificador de la prefencia
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setPreferenceS(Context context, int type,String key, String value){
        SharedPreferences sharedPreferences;

        switch (type){
            case TYPE_STANDARD:
                sharedPreferences = getSharedPreferencesN(context);
                break;
            case TYPE_REGISTER:
                sharedPreferences = getSharedPreferencesR(context);
                break;
            default:
                sharedPreferences = getSharedPreferencesN(context);
                break;
        }

        sharedPreferences.edit().putString(key, value).apply();
    }

    /**
     * Guarda el valor de una preferencia de cadena
     * @param context - en donde se ejecuta
     * @param key - identificador de la prefencia
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setPreferenceS(Context context, String key, String value){
        SharedPreferences sharedPreferences = getSharedPreferencesN(context);
        sharedPreferences.edit().putString(key, value).apply();
    }

    /**
     * Guarda el valor de una preferencia de cadena
     * @param context - en donde se ejecuta
     * @param key - identificador de la prefencia
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setPreferenceB(Context context, int type, String key, boolean value){
        SharedPreferences sharedPreferences;

        switch (type){
            case TYPE_STANDARD:
                sharedPreferences = getSharedPreferencesN(context);
                break;
            case TYPE_REGISTER:
                sharedPreferences = getSharedPreferencesR(context);
                break;
            default:
                sharedPreferences = getSharedPreferencesN(context);
                break;
        }

        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    /**
     * Guarda el valor de una preferencia booleana
     * @param context - en donde se ejecuta
     * @param key - identificador de la prefencia
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setPreferenceB(Context context, String key, boolean value){
        SharedPreferences sharedPreferences = getSharedPreferencesN(context);
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    /**
     * Guarda una prefencia en formato JSON
     * @param context - en donde se ejecuta
     * @param type - Tipo de preferencia que se va a usar
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setJSON(Context context, int type, String value){
        SharedPreferences sharedPreferences;

        switch (type){
            case TYPE_STANDARD:
                sharedPreferences = getSharedPreferencesN(context);
                break;
            case TYPE_REGISTER:
                sharedPreferences = getSharedPreferencesR(context);
                break;
            default:
                sharedPreferences = getSharedPreferencesN(context);
                break;
        }

        sharedPreferences.edit().putString(JSON_DATA, value).apply();
    }

    public static void setJSON2(Context context, String key, String value){
        SharedPreferences sharedPreferences;


        sharedPreferences = getSharedPreferencesN(context);
        sharedPreferences.edit().putString(key, value).apply();
    }

    /**
     * Obtiene el valor de una preferencia de cadena
     * @param context - en donde se ejecuta
     * @param type - Tipo de preferencia que se va a usar
     * @param key - identificador de la prefencia
     * @return - el contenido de la prefencia
     */
    public static String getPreferenceS(Context context, int type, String key) {
        SharedPreferences sharedPreferences;

        switch (type){
            case TYPE_STANDARD:
                sharedPreferences = getSharedPreferencesN(context);
                break;
            case TYPE_REGISTER:
                sharedPreferences = getSharedPreferencesR(context);
                break;
            default:
                sharedPreferences = getSharedPreferencesN(context);
                break;
        }

        return sharedPreferences.getString(key, "");
    }

    /**
     * Obtiene el valor de una preferencia de cadena
     * @param context - en donde se ejecuta
     * @param key - identificador de la prefencia
     * @return el contenido de la prefencia
     */
    public static String getPreferenceS(Context context, String key) {
        SharedPreferences sharedPreferences = getSharedPreferencesN(context);
        return sharedPreferences.getString(key, "");
    }

    /**
     * Obtiene el valor de una preferencia booleana
     * @param context - en donde se ejecuta
     * @param type - Tipo de preferencia que se va a usar
     * @param key - identificador de la prefencia
     * @return - el contenido de la prefencia
     */
    public static boolean getPreferenceB(Context context, int type, String key) {
        SharedPreferences sharedPreferences;

        switch (type){
            case TYPE_STANDARD:
                sharedPreferences = getSharedPreferencesN(context);
                break;
            case TYPE_REGISTER:
                sharedPreferences = getSharedPreferencesR(context);
                break;
            default:
                sharedPreferences = getSharedPreferencesN(context);
                break;
        }

        return sharedPreferences.getBoolean(key, false);
    }

    /**
     * Obtiene el valor de una preferencia booleana a partir del contexto y la key
     * @param context - en donde se ejecuta
     * @param key - identificador de la prefencia
     * @return - el contenido de la prefencia
     */
    public static boolean getPreferenceB(Context context, String key) {
        SharedPreferences sharedPreferences = getSharedPreferencesN(context);
        return sharedPreferences.getBoolean(key, false);
    }

    /**
     * Obtiene una prefencia en formato JSON
     * @param context - En donde se ejecuta
     * @return - el contenido de la prefencia
     */
    public static String getJSON(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferencesN(context);
        String frames = sharedPreferences.getString(JSON_DATA, "");
        System.out.println("ENCODE = " + frames);
        //frames = Utils.decryption(frames);
        System.out.println("DECODE = " + frames);

        return frames;
    }

    public static String getJSON2(Context context, String key) {
        SharedPreferences sharedPreferences = getSharedPreferencesN(context);
        String frames = sharedPreferences.getString(key, "");
        System.out.println("ENCODE = " + frames);
        //frames = Utils.decryption(frames);
        System.out.println("DECODE = " + frames);

        return frames;
    }

    /**
     * Inicializa el SharedPrefence
     * @param context - En donde se ejecuta
     * @return SharedPreference
     */
    private static SharedPreferences getSharedPreferencesN(Context context) {
        return context.getSharedPreferences(PREFERENCE_N, Context.MODE_PRIVATE);
    }

    /**
     * Inicializa el SharedPrefence
     * @param context - En donde se ejecuta
     * @return SharedPreference
     */
    private static SharedPreferences getSharedPreferencesR(Context context) {
        return context.getSharedPreferences(PREFERENCE_R, Context.MODE_PRIVATE);
    }

    /**
     * Elimina las preferencias STANDARD de la aplicacion
     * @param context - En donde se ejecuta
     */
    public static void closeSession(Context context){
        SharedPreferences sharedPreferences = getSharedPreferencesN(context);
        sharedPreferences.edit().clear().apply();
    }

    /**
     * Elimina las preferencias de REGISTRO de la aplicacion
     * @param context - En donde se ejecuta
     */
    public static void delectPreferenceR(Context context){
        SharedPreferences sharedPreferences = getSharedPreferencesR(context);
        sharedPreferences.edit().clear().apply();
    }
}