package com.mlaj.scard.Utils;

import android.support.annotation.Nullable;

//clase utilitaria
public class Utils {

    public static final String ID_USER = "id_user";
    public static final String NAME = "name";
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String SOCIAL_NETWORK = "social_network";
    public static final String PAY_TYPE = "pay_type";
    public static final String CODE = "code";
    public static final String LOADHOME = "load";
    public static final String LOADSECTION = "loadSection";
    public static final String STATUS = "status";
    public static final String FAV = "fav";

    public static final String WSHOME = "wsHome";

    //public static final String URL = "http://192.168.0.170/scard_app/public";
    public static final String URL = "https://salecard.co/public";
    public static final String URLIMG = URL + "/img/";

    //metodo que se reutiliza para cambiar el titulo del fragmento
    @Nullable
    public static String toTitleCase(@Nullable String str) {

        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }
        return builder.toString();
    }
}
