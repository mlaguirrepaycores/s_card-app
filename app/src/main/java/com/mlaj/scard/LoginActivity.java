package com.mlaj.scard;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mlaj.scard.Models.VolleySingleton;
import com.mlaj.scard.Utils.Preferences;
import com.mlaj.scard.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//Clase utilizada para iniciar sesión.
public class LoginActivity extends AppCompatActivity {

    EditText etLogCed, etLogPass;
    TextView tvNewReg;
    Button btnLog;

    JsonObjectRequest jsonObjectRequest;

    //ProgressDialog dialog;
    MyProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        //getSupportActionBar().hide();

        etLogCed = findViewById(R.id.etLogCed);
        etLogPass = findViewById(R.id.etLogPass);
        tvNewReg = findViewById(R.id.tvNewReg);
        btnLog = findViewById(R.id.btnLog);

        etLogCed.setText("9999");
        etLogPass.setText("1234");

        //tras confirmar los datos de inicio de sesión entrara a la mainActivity
        btnLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Se accede a la actividad MainActivity.
                String logCed = etLogCed.getText().toString();
                String logPass = etLogPass.getText().toString();
                //validación de ingreso de datos.
                if (logCed.length() != 0) {
                    if (logPass.length() != 0) {
                        //ejecución del webservice
                        cargarWebService();
                    } else {
                        //mensajes de error
                        Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //mensajes de error
                    Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //accion del click para ir a la sección de registro
        tvNewReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, Register1Activity.class);
                startActivity(i);
            }
        });
    }


    /**
     * Web Service de inicio de sesion
     */
    private void cargarWebService() {
        //Inicio de Progress Dialog
        dialog = new MyProgressDialog(this);
        dialog.setMessage("Comprobando ...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.show();

        /*
         * URL donde se encuentra alojado el Servicio.
         * Se envian como parametros el numero de identificación, la contraseña, ambos ingresados por el usuario
         * ademas se envia el token
         */

        String url = Utils.URL + "/wsJSONLogin.php?identifier=" + etLogCed.getText().toString() + "&password=" + etLogPass.getText().toString();

        //inicialización de JSON donde se pide un objeto a traves de un GET
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Se obtiene la respuesta del servicio
                JSONArray json = response.optJSONArray("client");
                JSONObject jsonObject = null;

                try {
                    jsonObject = json.getJSONObject(0);
                    if (jsonObject.getBoolean(Utils.STATUS)) {

                        //Si la respuesta es TRUE se asignan los valores a las Preferences
                        Preferences.setPreferenceS(LoginActivity.this, Utils.ID_USER, jsonObject.optString("id_user") + "");
                        Preferences.setPreferenceS(LoginActivity.this, Utils.NAME, jsonObject.optString("name"));
                        Preferences.setPreferenceS(LoginActivity.this, Utils.PHONE, jsonObject.optInt("phone") + "");
                        Preferences.setPreferenceS(LoginActivity.this, Utils.EMAIL, jsonObject.optString("email"));
                        Preferences.setPreferenceS(LoginActivity.this, Utils.PASSWORD, jsonObject.optString("password"));
                        Preferences.setPreferenceS(LoginActivity.this, Utils.SOCIAL_NETWORK, jsonObject.optInt("social_network") + "");
                        Preferences.setPreferenceS(LoginActivity.this, Utils.PAY_TYPE, jsonObject.optInt("pay_type") + "");
                        Preferences.setPreferenceS(LoginActivity.this, Utils.CODE, jsonObject.optInt("code") + "");
                        Preferences.setPreferenceS(LoginActivity.this, Utils.LOADHOME, "0");

                        //Se accede a la actividad MainActivity.
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));

                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    } else {
                        //Si la respuesta es FALSE se envia un mensaje de error.
                        Toast.makeText(getApplicationContext(), "Error en las credenciales", Toast.LENGTH_SHORT).show();
                    }
                    //Se finaliza el progress dialog
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //En caso de no poder acceder al servicio se muestra uno de los siguientes mensajes de error.
                dialog.dismiss();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getApplicationContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(), R.string.authFailureError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(), R.string.serverError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getApplicationContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(), R.string.parseError, Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Se agrega la petición
        VolleySingleton.getIntanciaVolley(this).addToRequestQueue(jsonObjectRequest);
    }
}
