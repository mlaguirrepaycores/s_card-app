package com.mlaj.scard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mlaj.scard.Models.User;

//Clase utilizada para realizar el primer paso del registro del usuario
public class Register1Activity extends AppCompatActivity {

    //elementos de la vista para ser usados
    EditText etReg1ID;
    EditText etReg1Pass;
    EditText etReg1Pass2;

    Button btnReg1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register1);

        //inicializacion de elementos a usar
        etReg1ID = (EditText) findViewById(R.id.etReg1ID);
        etReg1Pass = (EditText) findViewById(R.id.etReg1Pass);
        etReg1Pass2 = (EditText) findViewById(R.id.etReg1Pass2);
        this.setTitle("Registro");

        //Llenado de informacion para pruebas
        etReg1ID.setText("1094884614");
        etReg1Pass.setText("1234");
        etReg1Pass2.setText("1234");

        btnReg1 = (Button) findViewById(R.id.btnReg1);

        //Se agrega el listener al boton
        btnReg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User();
                String id_user = etReg1ID.getText().toString();
                String pass1 = etReg1Pass.getText().toString();
                String pass2 = etReg1Pass2.getText().toString();

                if (id_user.length() != 0 && pass1.length() != 0 && pass2.length() != 0) {
                    if (pass1.equals(pass2)) {
                        //Se agregan los datos ingresado al User y se envian como parametro a la siguiente actividad.
                        user.setId_user(id_user);
                        user.setPassword(pass1);
                        Intent i = new Intent(Register1Activity.this, QrReaderActivity.class);
                        i.putExtra("user", user);
                        startActivity(i);
                    } else {
                        //mensajes de error
                        Toast.makeText(getApplicationContext(), "Las contraseñas deben coincidir", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //mensajes de error
                    Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
