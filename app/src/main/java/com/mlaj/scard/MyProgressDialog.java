package com.mlaj.scard;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Leandro Aguirre.
 **/
public class MyProgressDialog extends ProgressDialog {
    public MyProgressDialog(Context context) {
        super(context,R.style.NewDialog);
    }
}
