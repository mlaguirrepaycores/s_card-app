package com.mlaj.scard.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mlaj.scard.Interfaces.FavAllyClickListener;
import com.mlaj.scard.Models.Ally;
import com.mlaj.scard.Models.FavSection;

import java.util.ArrayList;
import com.mlaj.scard.R;

//Clase que sirve de apadtador para el llenado de las secciones favoritas.
public class AdapterFavSections extends RecyclerView.Adapter<AdapterFavSections.ViewHolder>{

    private final ArrayList<FavSection> list;
    Context context;

    //constructor con parametros: lista.
    public AdapterFavSections(Context context,ArrayList<FavSection> list) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_fav_sections, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        FavSection favSection = list.get(position);
        String title = favSection.getFav_title()+": Mis Favoritos";
        final ArrayList<Ally> allies = favSection.getList();

        holder.tvFavSecName.setText(title);
        AdapterFavAlly adapterFavAlly = new AdapterFavAlly(context,allies, new FavAllyClickListener() {
            @Override
            public void onFavAllyClicked(FavAllyViewHolder holder, int position) {
                Ally ally = new Ally();
                ally = allies.get(position);

            }

        });

        holder.innerRecyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        manager.setReverseLayout(false);
        holder.innerRecyclerView.setLayoutManager(manager);
        holder.innerRecyclerView.setAdapter(adapterFavAlly);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView innerRecyclerView;
        TextView tvFavSecName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            innerRecyclerView = (RecyclerView) itemView.findViewById(R.id.rvFavAlly);
            tvFavSecName = (TextView) itemView.findViewById(R.id.tvFavSecName);
        }
    }
}
