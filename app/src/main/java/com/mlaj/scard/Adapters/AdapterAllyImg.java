package com.mlaj.scard.Adapters;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import com.github.snowdream.android.widget.SmartImageView;
import com.mlaj.scard.R;

//Clase que sirve de apadtador para el llenado de las imagenes informativas de cada tienda.
public class AdapterAllyImg extends RecyclerView.Adapter<AdapterAllyImg.ViewHolder> {

    ArrayList<String> list;

    //constructor con parametros: lista.
    public AdapterAllyImg(ArrayList<String> list) {
        this.list = list;
    }

    //Infla el contenedor con el layout list_ally_img
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ally_img, parent, false);

        return new ViewHolder(view);
    }

    //Llenado de informacion
    @Override
    public void onBindViewHolder(@NonNull AdapterAllyImg.ViewHolder holder, int position) {
        Rect rect = new Rect(holder.ivRecAlly.getLeft(), holder.ivRecAlly.getTop(), holder.ivRecAlly.getRight(), holder.ivRecAlly.getBottom());
        holder.ivRecAlly.setImageUrl(list.get(position), rect);
    }

    //tamaño del listado
    @Override
    public int getItemCount() {
        return list.size();
    }

    //vista del recycler donde se inicializan los elementos de la misma.
    public class ViewHolder extends RecyclerView.ViewHolder {

        SmartImageView ivRecAlly;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivRecAlly = (SmartImageView) itemView.findViewById(R.id.ivRecAlly);
        }
    }
}
