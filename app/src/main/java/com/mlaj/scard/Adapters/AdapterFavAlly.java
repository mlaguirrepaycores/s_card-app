package com.mlaj.scard.Adapters;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mlaj.scard.Fragments.AllyFragment;
import com.mlaj.scard.Interfaces.FavAllyClickListener;
import com.mlaj.scard.Models.Ally;
import com.mlaj.scard.R;

import java.util.ArrayList;

public class AdapterFavAlly extends RecyclerView.Adapter<FavAllyViewHolder>{
    private final ArrayList<Ally> list;
    private final FavAllyClickListener mListener;
    Context context;

    //constructor con parametros: lista y el Listener personalizado
    public AdapterFavAlly(Context context, ArrayList<Ally> list, FavAllyClickListener mListener) {
        this.context = context;
        this.list = list;
        this.mListener = mListener;
    }

    //Infla el contenedor con el layout grid_item
    @NonNull
    @Override
    public FavAllyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_fav_ally, parent, false);
        return new FavAllyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavAllyViewHolder holder, final int position) {
        ViewCompat.setTransitionName(holder.imgFavAlly, String.valueOf(position)+"_image2");
        Rect rect = new Rect(holder.imgFavAlly.getLeft(), holder.imgFavAlly.getTop(), holder.imgFavAlly.getRight(), holder.imgFavAlly.getBottom());
        String url = list.get(position).getLogo();
        holder.tvFavAlly.setText(list.get(position).getName());
        holder.imgFavAlly.setImageUrl(url, rect);
        holder.imgFavAlly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onFavAllyClicked(holder, position);
                //Toast.makeText(context, "Prueba", Toast.LENGTH_SHORT).show();
                AllyFragment fragment = AllyFragment.newInstance(list.get(position));

                AppCompatActivity activity = (AppCompatActivity) context;
                activity.getSupportFragmentManager()
                        .beginTransaction()
                        //.addSharedElement(holder.imgFavAlly, "exampleT")
                        .replace(R.id.content_main, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
