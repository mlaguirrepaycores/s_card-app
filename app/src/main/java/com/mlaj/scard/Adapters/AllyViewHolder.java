package com.mlaj.scard.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.github.snowdream.android.widget.SmartImageView;
import com.mlaj.scard.R;

//vista del recycler donde se inicializan los elementos de la misma.
public class AllyViewHolder extends RecyclerView.ViewHolder {
    public SmartImageView imgGrid;
    public TextView tvNameGrid;
    public AllyViewHolder(@NonNull View itemView) {
        super(itemView);
        this.imgGrid = (SmartImageView) itemView.findViewById(R.id.imgGrid);
        this.tvNameGrid = (TextView) itemView.findViewById(R.id.tvNameGrid);
    }
}
