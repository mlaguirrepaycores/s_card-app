package com.mlaj.scard.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.github.snowdream.android.widget.SmartImageView;
import com.mlaj.scard.R;

//vista del recycler donde se inicializan los elementos de la misma.
public class SectionViewHolder extends RecyclerView.ViewHolder {

    public TextView tvSecName;
    public TextView tvSecDesc;
    public TextView tvSecAllyHome;
    public SmartImageView imgSection;

    public SectionViewHolder(@NonNull View itemView) {
        super(itemView);
        this.tvSecName = (TextView) itemView.findViewById(R.id.tvSecNameHome);
        this.tvSecDesc = (TextView) itemView.findViewById(R.id.tvSecDescHome);
        this.tvSecAllyHome = (TextView) itemView.findViewById(R.id.tvSecAllyHome);
        this.imgSection = (SmartImageView) itemView.findViewById(R.id.imgSectionHome);
    }
}
