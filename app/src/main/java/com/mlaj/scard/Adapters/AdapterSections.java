package com.mlaj.scard.Adapters;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mlaj.scard.Interfaces.SectionClickListener;
import com.mlaj.scard.Models.Section;
import com.mlaj.scard.R;

import java.util.ArrayList;

//Clase que sirve de apadtador para el llenado de las secciones.
public class AdapterSections extends RecyclerView.Adapter<SectionViewHolder> {

    private final ArrayList<Section> list;
    private final SectionClickListener mlistener;

    //constructor con parametros: lista y el Listener personalizado
    public AdapterSections(ArrayList<Section> list, SectionClickListener mlistener) {
        this.list = list;
        this.mlistener = mlistener;
    }

    //Infla el contenedor con el layout list_sections
    @NonNull
    @Override
    public SectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_sections, parent, false);
        return new SectionViewHolder(view);
    }

    //Llenado de informacion
    @Override
    public void onBindViewHolder(@NonNull final SectionViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.tvSecName.setText(list.get(position).getName());
        String allies = list.get(position).getAllies() + " Aliados en todo el pais";
        holder.tvSecAllyHome.setText(allies);
        holder.tvSecDesc.setText(list.get(position).getDesc());

        String url = list.get(position).getImage();

        Rect rect = new Rect(holder.imgSection.getLeft(), holder.imgSection.getTop(), holder.imgSection.getRight(), holder.imgSection.getBottom());

        holder.imgSection.setImageUrl(url, rect);
        ViewCompat.setTransitionName(holder.imgSection, String.valueOf(position) + "_image1");
        holder.imgSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mlistener.onSectionClicked(holder, position);
            }
        });
    }

    //tamaño del listado
    @Override
    public int getItemCount() {
        return list.size();
    }
}
