package com.mlaj.scard.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.github.snowdream.android.widget.SmartImageView;
import com.mlaj.scard.R;

public class FavAllyViewHolder extends RecyclerView.ViewHolder {

    public SmartImageView imgFavAlly;
    public TextView tvFavAlly;

    public FavAllyViewHolder(@NonNull View itemView) {
        super(itemView);
        this.imgFavAlly = (SmartImageView) itemView.findViewById(R.id.imgFavAlly);
        this.tvFavAlly = (TextView) itemView.findViewById(R.id.tvFavAlly);
    }
}
