package com.mlaj.scard.Adapters;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mlaj.scard.Interfaces.AllyClickListener;
import com.mlaj.scard.Models.Ally;
import com.mlaj.scard.R;

import java.util.ArrayList;
import java.util.Locale;

//Clase que sirve de apadtador para el llenado de las tiendas.
public class AdapterSearch extends RecyclerView.Adapter<AllyViewHolder> {
    private final ArrayList<Ally> list;
    private final ArrayList<Ally> arraylist;
    private final AllyClickListener mListener;

    //constructor con parametros: lista y el Listener personalizado
    public AdapterSearch(ArrayList<Ally> list, AllyClickListener mListener) {
        this.list = list;
        this.mListener = mListener;
        arraylist = new ArrayList<Ally>();
        arraylist.addAll(list);
        list.clear();
    }

    //Infla el contenedor con el layout grid_item
    @NonNull
    @Override
    public AllyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);
        return new AllyViewHolder(view);
    }

    //Llenado de informacion
    @Override
    public void onBindViewHolder(@NonNull final AllyViewHolder holder, final int position) {
        ViewCompat.setTransitionName(holder.imgGrid, String.valueOf(position) + "_image");

        Ally ally = list.get(position);
        Rect rect = new Rect(holder.imgGrid.getLeft(), holder.imgGrid.getTop(), holder.imgGrid.getRight(), holder.imgGrid.getBottom());
        String url = ally.getLogo();
        holder.imgGrid.setImageUrl(url, rect);
        holder.imgGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onAllyClicked(holder, position);
            }
        });
        holder.tvNameGrid.setText(ally.getName());
    }

    //tamaño del listado
    @Override
    public int getItemCount() {
        return list.size();
    }

    //metodo usado para realizar busquedas
    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.clear();

        } else {
            for (Ally AllyDetail : arraylist) {
                if (charText.length() != 0 && AllyDetail.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    list.add(AllyDetail);
                }
            }
        }
        notifyDataSetChanged();
    }
}
