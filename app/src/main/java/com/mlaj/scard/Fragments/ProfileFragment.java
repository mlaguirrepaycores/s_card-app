package com.mlaj.scard.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mlaj.scard.R;
import com.mlaj.scard.Utils.Preferences;
import com.mlaj.scard.Utils.Utils;

//Clase donde se visualiza la informacion del cliente
public class ProfileFragment extends Fragment {

    TextView tvProfName;
    TextView tvProfEmail;
    TextView tvProfPhone;

    private OnFragmentInteractionListener mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        getActivity().setTitle(Utils.toTitleCase(getString(R.string.profile)));

        tvProfName = view.findViewById(R.id.tvProfName);
        tvProfEmail = view.findViewById(R.id.tvProfEmail);
        tvProfPhone = view.findViewById(R.id.tvProfPhone);

        tvProfName.setText(Preferences.getPreferenceS(getContext(), Utils.NAME));
        tvProfEmail.setText(Preferences.getPreferenceS(getContext(), Utils.EMAIL));
        tvProfPhone.setText(Preferences.getPreferenceS(getContext(), Utils.PHONE));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
