package com.mlaj.scard.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.snowdream.android.widget.SmartImageView;
import com.mlaj.scard.Adapters.AdapterAlly;
import com.mlaj.scard.Adapters.AllyViewHolder;
import com.mlaj.scard.DetailsTransition;
import com.mlaj.scard.Interfaces.AllyClickListener;
import com.mlaj.scard.Models.Ally;
import com.mlaj.scard.Models.Section;
import com.mlaj.scard.Models.VolleySingleton;
import com.mlaj.scard.MyProgressDialog;
import com.mlaj.scard.R;
import com.mlaj.scard.Utils.Preferences;
import com.mlaj.scard.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//Fragmento donde se visualiza la informacion de una seccion especifica
//y se enlistan sus tiendas
public class SectionFragment extends Fragment implements AllyClickListener {

    //Inicializacion de paramentros que se reciben o envian como argumentos
    private static final String ARG_PARAM1 = "name";

    TextView tvSecDesc;
    SmartImageView imgSection;
    AdapterAlly adapter;
    Section section;
    String section_id;
    JSONObject obj = null;
    ArrayList<Ally> list;
    RecyclerView recyclerView;
    GridLayoutManager manager;
    JsonObjectRequest jsonObjectRequest;
    MyProgressDialog dialog;

    private int previousItemCount;
    public boolean isLoading = false;
    String user_id;

    private JSONArray jsonArray;
    public Handler mHandler;
    @Nullable
    private OnFragmentInteractionListener mListener;

    public SectionFragment() {
        // Required empty public constructor
    }

    //Metodo autogenerado y personalizado que se usa para la creacion de una nueva instancia.
    // TODO: Rename and change types and number of parameters
    @NonNull
    public static SectionFragment newInstance(Section section) {
        SectionFragment fragment = new SectionFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, section);
        fragment.setArguments(args);
        return fragment;
    }

    //Se obtienen los valores de los argumentos que fueron enviados al crear la nueva instancia.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            section = (Section) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    //Se infla el contenedor con el nuevo fragmento
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //infla el Layout para este fragmento
        View view = inflater.inflate(R.layout.fragment_section, container, false);
        //Cambio de titulo en la barra superior
        getActivity().setTitle(Utils.toTitleCase(section.getName()));
        user_id = Preferences.getPreferenceS(getActivity(), Utils.ID_USER);

        imgSection = view.findViewById(R.id.imgSection);
        tvSecDesc = view.findViewById(R.id.tvSecDesc);

        Rect rect = new Rect(imgSection.getLeft(), imgSection.getTop(), imgSection.getRight(), imgSection.getBottom());
        imgSection.setImageUrl(section.getImage(), rect);
        tvSecDesc.setText(section.getDesc());

        mHandler = new MyHandler();
        section_id = section.getId_section();
        list = new ArrayList<>();
        String load = Preferences.getPreferenceS(getContext(), Utils.LOADSECTION + section_id);

        if (load.equals("1")) {
            allyWebService();
        } else {
            String object = Preferences.getJSON2(getContext(), section_id);
            try {
                obj = new JSONObject(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray json = obj.optJSONArray("client");
            try {
                for (int i = 0; i < json.length(); i++) {
                    final Ally ally = new Ally();
                    String favorite;

                    //Se asignan valores obtenidos del servicio
                    String id_ally = json.getJSONObject(i).getString("id_ally");
                    String name = json.getJSONObject(i).getString("name");
                    String logo = json.getJSONObject(i).getString("logo");
                    String title = json.getJSONObject(i).getString("title");
                    String desc = json.getJSONObject(i).getString("descr");
                    String facebook = json.getJSONObject(i).getString("facebook");
                    String instagram = json.getJSONObject(i).getString("instagram");
                    String phone = json.getJSONObject(i).getString("phone");
                    String email = json.getJSONObject(i).getString("email");
                    String location = json.getJSONObject(i).getString("location");
                    String website = json.getJSONObject(i).getString("website");
                    String prom_item = json.getJSONObject(i).getString("prom_item");
                    String prom_item_value = json.getJSONObject(i).getString("prom_item_value");
                    String prom_desc = json.getJSONObject(i).getString("prom_desc");
                    String news_title = json.getJSONObject(i).getString("news_title");
                    String news_desc = json.getJSONObject(i).getString("news_desc");
                    String image1 = json.getJSONObject(i).getString("image1");
                    String image2 = json.getJSONObject(i).getString("image2");
                    String image3 = json.getJSONObject(i).getString("image3");
                    String image4 = json.getJSONObject(i).getString("image4");
                    String image5 = json.getJSONObject(i).getString("image5");
                    String section_id_section = json.getJSONObject(i).getString("section_id_section");
                    String ally_fav = json.getJSONObject(i).getString("user_id_user");
                    String iterator = i + "";

                    String url = Utils.URLIMG + "ally/" + logo;
                    url = url.replace(" ", "%20");

                    if (ally_fav.equals(user_id)) {
                        favorite = "1";
                    } else {
                        favorite = "0";
                    }

                    Preferences.setPreferenceS(getContext(), Utils.LOADSECTION + section_id, "0");

                    ally.setId_ally(id_ally);
                    ally.setName(name);
                    ally.setLogo(url);
                    ally.setTitle(title);
                    ally.setDesc(desc);
                    ally.setFacebook(facebook);
                    ally.setInstagram(instagram);
                    ally.setPhone(phone);
                    ally.setEmail(email);
                    ally.setLocation(location);
                    ally.setWebsite(website);
                    ally.setProm_item(prom_item);
                    ally.setProm_item_value(prom_item_value);
                    ally.setProm_desc(prom_desc);
                    ally.setNews_title(news_title);
                    ally.setNews_desc(news_desc);
                    ally.setImage1(image1);
                    ally.setImage2(image2);
                    ally.setImage3(image3);
                    ally.setImage4(image4);
                    ally.setImage5(image5);
                    ally.setSection_id_section(section_id_section);
                    ally.setAlly_fav(favorite);
                    ally.setIterator(iterator);
                    list.add(ally);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return view;
    }

    //metodo sobrecargado donde se llena el recyclerview
    //este metodo solo se usa cuando se quiere implementar el efecto de imagenes
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        manager = new GridLayoutManager(getContext(), 3);
        recyclerView = view.findViewById(R.id.rvGridAlly);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);


        adapter = new AdapterAlly(list, new AllyClickListener() {

            //metodo implementado del AllyClickListener
            @Override
            public void onAllyClicked(@NonNull AllyViewHolder holder, int position) {
                AllyFragment fragment = AllyFragment.newInstance(list.get(position));

                // Verifica que la api sea mayor a 21 y que tiene los permisos respectivos
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    fragment.setSharedElementEnterTransition(new DetailsTransition());
                    fragment.setEnterTransition(new Fade());
                    setExitTransition(new Fade());
                    fragment.setSharedElementReturnTransition(new DetailsTransition());
                }

                //Se inicia el siguiente fragmento y se usa el addSharedElement para crear el efecto de movimiento de la imagen
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addSharedElement(holder.imgGrid, getString(R.string.exampleT))
                        .replace(R.id.content_main, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        recyclerView.setAdapter(adapter);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int itemCount = manager.getItemCount();

                    if (itemCount != previousItemCount) {
                        isLoading = false;
                    }

                    if (!isLoading && manager.findLastCompletelyVisibleItemPosition() >= itemCount - 1) {
                                    /*previousItemCount = itemCount;
                                    isLoading = dataLoader.onLoadMore();*/
                        Thread thread = new ThreadGetMoreData();
                        thread.start();
                        isLoading = true;
                    }
                }
            }
        });
    }

    /**
     * Web Service que carga los aliados
     */

    private void allyWebService() {
        //Inicio de Progress Dialog
        dialog = new MyProgressDialog(getContext());
        dialog.setMessage("Comprobando ...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.show();

        //URL donde se encuentra alojado el Servicio.

        String url = Utils.URL + "/wsJSONAlly.php?id_section=" + section_id + "&user_id=" + user_id;

        //inicialización de JSON donde se pide un objeto a traves de un GET
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Se obtiene la respuesta del servicio
                JSONArray json2 = response.optJSONArray("client");
                jsonArray = response.optJSONArray("client");
                //se guarda la información del servicio en una Preferencia para ser usada de nuevo sin cargar el servicio
                Preferences.setJSON2(getContext(),section_id, response.toString());

                String object = Preferences.getJSON2(getContext(), section_id);
                try {
                    obj = new JSONObject(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONArray json = obj.optJSONArray("client");
                try {
                    for (int i = 0; i < 15; i++) {
                        final Ally ally = new Ally();
                        String favorite;

                        //Se asignan valores obtenidos del servicio
                        String id_ally = json.getJSONObject(i).getString("id_ally");
                        String name = json.getJSONObject(i).getString("name");
                        String logo = json.getJSONObject(i).getString("logo");
                        String title = json.getJSONObject(i).getString("title");
                        String desc = json.getJSONObject(i).getString("descr");
                        String facebook = json.getJSONObject(i).getString("facebook");
                        String instagram = json.getJSONObject(i).getString("instagram");
                        String phone = json.getJSONObject(i).getString("phone");
                        String email = json.getJSONObject(i).getString("email");
                        String location = json.getJSONObject(i).getString("location");
                        String website = json.getJSONObject(i).getString("website");
                        String prom_item = json.getJSONObject(i).getString("prom_item");
                        String prom_item_value = json.getJSONObject(i).getString("prom_item_value");
                        String prom_desc = json.getJSONObject(i).getString("prom_desc");
                        String news_title = json.getJSONObject(i).getString("news_title");
                        String news_desc = json.getJSONObject(i).getString("news_desc");
                        String image1 = json.getJSONObject(i).getString("image1");
                        String image2 = json.getJSONObject(i).getString("image2");
                        String image3 = json.getJSONObject(i).getString("image3");
                        String image4 = json.getJSONObject(i).getString("image4");
                        String image5 = json.getJSONObject(i).getString("image5");
                        String section_id_section = json.getJSONObject(i).getString("section_id_section");
                        String ally_fav = json.getJSONObject(i).getString("user_id_user");
                        String iterator = i + "";

                        String url = Utils.URLIMG + "ally/" + logo;
                        url = url.replace(" ", "%20");

                        if (ally_fav.equals(user_id)) {
                            favorite = "1";
                        } else {
                            favorite = "0";
                        }


                        ally.setId_ally(id_ally);
                        ally.setName(name);
                        ally.setLogo(url);
                        ally.setTitle(title);
                        ally.setDesc(desc);
                        ally.setFacebook(facebook);
                        ally.setInstagram(instagram);
                        ally.setPhone(phone);
                        ally.setEmail(email);
                        ally.setLocation(location);
                        ally.setWebsite(website);
                        ally.setProm_item(prom_item);
                        ally.setProm_item_value(prom_item_value);
                        ally.setProm_desc(prom_desc);
                        ally.setNews_title(news_title);
                        ally.setNews_desc(news_desc);
                        ally.setImage1(image1);
                        ally.setImage2(image2);
                        ally.setImage3(image3);
                        ally.setImage4(image4);
                        ally.setImage5(image5);
                        ally.setSection_id_section(section_id_section);
                        ally.setAlly_fav(favorite);
                        ally.setIterator(iterator);
                        list.add(ally);
                    }

                    Preferences.setPreferenceS(getContext(), Utils.LOADSECTION + section_id, "0");
                    adapter = new AdapterAlly(list, new AllyClickListener() {

                        //metodo implementado del AllyClickListener
                        @Override
                        public void onAllyClicked(@NonNull AllyViewHolder holder, int position) {
                            AllyFragment fragment = AllyFragment.newInstance(list.get(position));

                            // Verifica que la api sea mayor a 21 y que tiene los permisos respectivos
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                fragment.setSharedElementEnterTransition(new DetailsTransition());
                                fragment.setEnterTransition(new Fade());
                                setExitTransition(new Fade());
                                fragment.setSharedElementReturnTransition(new DetailsTransition());
                            }

                            //Se inicia el siguiente fragmento y se usa el addSharedElement para crear el efecto de movimiento de la imagen
                            getActivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .addSharedElement(holder.imgGrid, getString(R.string.exampleT))
                                    .replace(R.id.content_main, fragment)
                                    .addToBackStack(null)
                                    .commit();
                        }
                    });

                    manager = new GridLayoutManager(getContext(), 3);
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setAdapter(adapter);

                    recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                        }

                        @Override
                        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            if (dy > 0) {
                                int itemCount = manager.getItemCount();

                                if (itemCount != previousItemCount) {
                                    isLoading = false;
                                }

                                if (!isLoading && manager.findLastCompletelyVisibleItemPosition() >= itemCount - 1) {
                                    Thread thread = new ThreadGetMoreData();
                                    thread.start();
                                    isLoading = true;
                                }
                            }
                        }
                    });

                    //Se finaliza el progress dialog
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }

                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //En caso de no poder acceder al servicio se muestra uno de los siguientes mensajes de error.
                dialog.dismiss();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getContext(), R.string.authFailureError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getContext(), R.string.serverError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getContext(), R.string.parseError, Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Se agrega la petición
        VolleySingleton.getIntanciaVolley(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    //metodo implementado del AllyClickListener
    @Override
    public void onAllyClicked(@NonNull AllyViewHolder holder, int position) {
        AllyFragment fragment = AllyFragment.newInstance(list.get(position));

        // Verifica que la api sea mayor a 21 y que tiene los permisos respectivos
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setSharedElementEnterTransition(new DetailsTransition());
            fragment.setEnterTransition(new Fade());
            setExitTransition(new Fade());
            fragment.setSharedElementReturnTransition(new DetailsTransition());
        }

        //Se inicia el siguiente fragmento y se usa el addSharedElement para crear el efecto de movimiento de la imagen
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .addSharedElement(holder.imgGrid, getString(R.string.exampleT))
                .replace(R.id.content_main, fragment)
                .addToBackStack(null)
                .commit();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    //add loading view during search processing
                    //rvGridAlly.addFooterView(ftView);

                    break;
                case 1:
                    //update data adapter and UI
                    adapter.addListItemToAdapter((ArrayList<Ally>) msg.obj);
                    //remove loading view after update listview
                    //rvGridAlly.removeFooterView(ftView);
                    isLoading = false;
                    break;
                default:
                    break;

            }
        }
    }

    private ArrayList<Ally> getMoreData() {
        ArrayList<Ally> allies = new ArrayList<>();
        //Listado

        JSONArray list = new JSONArray();

        try {
            if (jsonArray != null) {
                int len = jsonArray.length();
                for (int i = 15; i < len; i++) {
                    list.put(jsonArray.get(i));
                }

                if (jsonArray.length() >= 15){

                    try {
                        for (int i = 0; i < 15; i++) {
                            final Ally ally = new Ally();
                            String favorite;

                            String id_ally = list.getJSONObject(i).getString("id_ally");
                            String name = list.getJSONObject(i).getString("name");
                            String logo = list.getJSONObject(i).getString("logo");
                            String title = list.getJSONObject(i).getString("title");
                            String desc = list.getJSONObject(i).getString("descr");
                            String facebook = list.getJSONObject(i).getString("facebook");
                            String instagram = list.getJSONObject(i).getString("instagram");
                            String phone = list.getJSONObject(i).getString("phone");
                            String email = list.getJSONObject(i).getString("email");
                            String location = list.getJSONObject(i).getString("location");
                            String website = list.getJSONObject(i).getString("website");
                            String prom_item = list.getJSONObject(i).getString("prom_item");
                            String prom_item_value = list.getJSONObject(i).getString("prom_item_value");
                            String prom_desc = list.getJSONObject(i).getString("prom_desc");
                            String news_title = list.getJSONObject(i).getString("news_title");
                            String news_desc = list.getJSONObject(i).getString("news_desc");
                            String image1 = list.getJSONObject(i).getString("image1");
                            String image2 = list.getJSONObject(i).getString("image2");
                            String image3 = list.getJSONObject(i).getString("image3");
                            String image4 = list.getJSONObject(i).getString("image4");
                            String image5 = list.getJSONObject(i).getString("image5");
                            String section_id_section = list.getJSONObject(i).getString("section_id_section");
                            String ally_fav = list.getJSONObject(i).getString("user_id_user");
                            String iterator = i + "";

                            String url = "http://192.168.0.170/scard_app/public/img/ally/" + logo;
                            //String url = "http://salecard.co/public/img/ally/" + logo;
                            url = url.replace(" ", "%20");

                            if (ally_fav.equals(1094)) {

                                favorite = "1";
                            } else {
                                favorite = "0";
                            }

                            ally.setId_ally(id_ally);
                            ally.setName(name);
                            ally.setLogo(url);
                            ally.setTitle(title);
                            ally.setDesc(desc);
                            ally.setFacebook(facebook);
                            ally.setInstagram(instagram);
                            ally.setPhone(phone);
                            ally.setEmail(email);
                            ally.setLocation(location);
                            ally.setWebsite(website);
                            ally.setProm_item(prom_item);
                            ally.setProm_item_value(prom_item_value);
                            ally.setProm_desc(prom_desc);
                            ally.setNews_title(news_title);
                            ally.setNews_desc(news_desc);
                            ally.setImage1(image1);
                            ally.setImage2(image2);
                            ally.setImage3(image3);
                            ally.setImage4(image4);
                            ally.setImage5(image5);
                            ally.setSection_id_section(section_id_section);
                            ally.setAlly_fav(favorite);
                            ally.setIterator(iterator);
                            allies.add(ally);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (jsonArray.length() < 15){

                    try {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            final Ally ally = new Ally();
                            String favorite;

                            String id_ally = list.getJSONObject(i).getString("id_ally");
                            String name = list.getJSONObject(i).getString("name");
                            String logo = list.getJSONObject(i).getString("logo");
                            String title = list.getJSONObject(i).getString("title");
                            String desc = list.getJSONObject(i).getString("descr");
                            String facebook = list.getJSONObject(i).getString("facebook");
                            String instagram = list.getJSONObject(i).getString("instagram");
                            String phone = list.getJSONObject(i).getString("phone");
                            String email = list.getJSONObject(i).getString("email");
                            String location = list.getJSONObject(i).getString("location");
                            String website = list.getJSONObject(i).getString("website");
                            String prom_item = list.getJSONObject(i).getString("prom_item");
                            String prom_item_value = list.getJSONObject(i).getString("prom_item_value");
                            String prom_desc = list.getJSONObject(i).getString("prom_desc");
                            String news_title = list.getJSONObject(i).getString("news_title");
                            String news_desc = list.getJSONObject(i).getString("news_desc");
                            String image1 = list.getJSONObject(i).getString("image1");
                            String image2 = list.getJSONObject(i).getString("image2");
                            String image3 = list.getJSONObject(i).getString("image3");
                            String image4 = list.getJSONObject(i).getString("image4");
                            String image5 = list.getJSONObject(i).getString("image5");
                            String section_id_section = list.getJSONObject(i).getString("section_id_section");
                            String ally_fav = list.getJSONObject(i).getString("user_id_user");
                            String iterator = i + "";


                            String url = "http://192.168.0.170/scard_app/public/img/ally/" + logo;
                            //String url = "http://salecard.co/public/img/ally/" + logo;
                            url = url.replace(" ", "%20");

                            if (ally_fav.equals(1094)) {

                                favorite = "1";
                            } else {
                                favorite = "0";
                            }

                            ally.setId_ally(id_ally);
                            ally.setName(name);
                            ally.setLogo(url);
                            ally.setTitle(title);
                            ally.setDesc(desc);
                            ally.setFacebook(facebook);
                            ally.setInstagram(instagram);
                            ally.setPhone(phone);
                            ally.setEmail(email);
                            ally.setLocation(location);
                            ally.setWebsite(website);
                            ally.setProm_item(prom_item);
                            ally.setProm_item_value(prom_item_value);
                            ally.setProm_desc(prom_desc);
                            ally.setNews_title(news_title);
                            ally.setNews_desc(news_desc);
                            ally.setImage1(image1);
                            ally.setImage2(image2);
                            ally.setImage3(image3);
                            ally.setImage4(image4);
                            ally.setImage5(image5);
                            ally.setSection_id_section(section_id_section);
                            ally.setAlly_fav(favorite);
                            ally.setIterator(iterator);
                            allies.add(ally);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        jsonArray = list;
        return allies;
    }

    public class ThreadGetMoreData extends Thread {
        @Override
        public void run() {
            //Inicio de Progress Dialog
            //Add footerView after get data
            mHandler.sendEmptyMessage(0);
            //search more data
            ArrayList<Ally> arrayList = getMoreData();
            //Delay time to show loading footer when debug, remove it when release
            //Send the result to handle
            Message msg = mHandler.obtainMessage(1, arrayList);
            mHandler.sendMessage(msg);
        }
    }
}
