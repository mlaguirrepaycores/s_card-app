package com.mlaj.scard.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mlaj.scard.Adapters.AdapterFavSections;
import com.mlaj.scard.Adapters.FavAllyViewHolder;
import com.mlaj.scard.Interfaces.FavAllyClickListener;
import com.mlaj.scard.Models.Ally;
import com.mlaj.scard.Models.FavSection;
import com.mlaj.scard.Models.Section;
import com.mlaj.scard.Models.VolleySingleton;
import com.mlaj.scard.R;
import com.mlaj.scard.Utils.Preferences;
import com.mlaj.scard.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//Clase donde se verifican cuales son los aliados favoritos y se muestran por seccion.
public class FavoritesFragment extends Fragment implements FavAllyClickListener {

    ArrayList<Section> list;
    ArrayList<FavSection> arrayList;

    RecyclerView recyclerView;
    AdapterFavSections adapterFavSections;
    JsonObjectRequest jsonObjectRequest;

    JSONObject obj = null;
    String user_id;

    private OnFragmentInteractionListener mListener;

    public FavoritesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        getActivity().setTitle(Utils.toTitleCase(getString(R.string.favorites)));

        user_id = Preferences.getPreferenceS(getActivity(), Utils.ID_USER);
        recyclerView = view.findViewById(R.id.rvFavorites);
        cargarWebServiceFav();
        return view;
    }

    private void cargarWebServiceFav() {

        //URL donde se encuentra alojado el Servicio.
        String url = Utils.URL + "/wsJSONFav.php?user_id_user=" + user_id;
        list = new ArrayList<>();

        arrayList = new ArrayList<>();
        //inicialización de JSON donde se pide un objeto a traves de un GET
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Obtengo el JSON de sections desde las preferencias y creo las secciones.
                String sections = Preferences.getJSON2(getContext(), Utils.WSHOME);
                try {
                    obj = new JSONObject(sections);

                    JSONArray json = obj.optJSONArray("client");
                    int countLoad = json.length();
                    for (int i = 0; i < countLoad; i++) {
                        String id_section;
                        String image;
                        String name;
                        String desc;
                        String count;

                        String url = Utils.URLIMG + json.getJSONObject(i).getString("image");
                        url = url.replace(" ", "%20");

                        id_section = json.getJSONObject(i).getString("id_section");
                        image = url;
                        name = json.getJSONObject(i).getString("name");
                        desc = json.getJSONObject(i).getString("desc");
                        count = json.getJSONObject(i).getString("allies");

                        Section section = new Section();
                        section.setId_section(id_section);
                        section.setImage(image);
                        section.setName(name);
                        section.setDesc(desc);
                        section.setAllies(count);
                        list.add(section);

                        //TODO seccion de llenado de aliados favoritos
                        final ArrayList<Ally> listAlly;

                        listAlly = new ArrayList<>();

                        JSONArray jsonFav = response.optJSONArray("client");

                        for (int j = 0; j < jsonFav.length(); j++) {

                            String favorite;
                            String section_id_section;
                            section_id_section = jsonFav.getJSONObject(j).getString("section_id_section");
                            if (section.getId_section().equals(section_id_section)) {

                                String id_ally;
                                String name_ally;
                                String logo;
                                String title;
                                String desc_ally;
                                String facebook;
                                String instagram;
                                String phone;
                                String email;
                                String location;
                                String website;
                                String prom_item;
                                String prom_item_value;
                                String prom_desc;
                                String news_title;
                                String news_desc;
                                String image1;
                                String image2;
                                String image3;
                                String image4;
                                String image5;
                                String ally_fav;
                                String iterator;

                                id_ally = jsonFav.getJSONObject(j).getString("id_ally");
                                name_ally = jsonFav.getJSONObject(j).getString("name");
                                logo = jsonFav.getJSONObject(j).getString("logo");
                                title = jsonFav.getJSONObject(j).getString("title");
                                desc_ally = jsonFav.getJSONObject(j).getString("descr");
                                facebook = jsonFav.getJSONObject(j).getString("facebook");
                                instagram = jsonFav.getJSONObject(j).getString("instagram");
                                phone = jsonFav.getJSONObject(j).getString("phone");
                                email = jsonFav.getJSONObject(j).getString("email");
                                location = jsonFav.getJSONObject(j).getString("location");
                                website = jsonFav.getJSONObject(j).getString("website");
                                prom_item = jsonFav.getJSONObject(j).getString("prom_item");
                                prom_item_value = jsonFav.getJSONObject(j).getString("prom_item_value");
                                prom_desc = jsonFav.getJSONObject(j).getString("prom_desc");
                                news_title = jsonFav.getJSONObject(j).getString("news_title");
                                news_desc = jsonFav.getJSONObject(j).getString("news_desc");
                                image1 = jsonFav.getJSONObject(j).getString("image1");
                                image2 = jsonFav.getJSONObject(j).getString("image2");
                                image3 = jsonFav.getJSONObject(j).getString("image3");
                                image4 = jsonFav.getJSONObject(j).getString("image4");
                                image5 = jsonFav.getJSONObject(j).getString("image5");
                                ally_fav = jsonFav.getJSONObject(j).getString("user_id_user");
                                iterator = j + "";

                                String url2 = Utils.URLIMG + "ally/" + logo;
                                url2 = url2.replace(" ", "%20");

                                if (ally_fav.equals(user_id)) {
                                    favorite = "1";
                                } else {
                                    favorite = "0";
                                }

                                Ally ally = new Ally();
                                ally.setId_ally(id_ally);
                                ally.setName(name_ally);
                                ally.setLogo(url2);
                                ally.setTitle(title);
                                ally.setDesc(desc_ally);
                                ally.setFacebook(facebook);
                                ally.setInstagram(instagram);
                                ally.setPhone(phone);
                                ally.setEmail(email);
                                ally.setLocation(location);
                                ally.setWebsite(website);
                                ally.setProm_item(prom_item);
                                ally.setProm_item_value(prom_item_value);
                                ally.setProm_desc(prom_desc);
                                ally.setNews_title(news_title);
                                ally.setNews_desc(news_desc);
                                ally.setImage1(image1);
                                ally.setImage2(image2);
                                ally.setImage3(image3);
                                ally.setImage4(image4);
                                ally.setImage5(image5);
                                ally.setSection_id_section(section_id_section);
                                ally.setAlly_fav(favorite);
                                ally.setIterator(iterator);

                                listAlly.add(ally);
                            }
                        }

                        FavSection favSection = new FavSection();
                        favSection.setFav_title(section.getName());
                        favSection.setList(listAlly);
                        if (favSection.getList().size() != 0) {
                            arrayList.add(favSection);
                        }

                    }

                    Log.e("ARRAYLIST", arrayList.toString());
                    LinearLayoutManager manager = new LinearLayoutManager(getContext());
                    manager.setOrientation(LinearLayoutManager.VERTICAL);
                    manager.setReverseLayout(false);
                    recyclerView.setLayoutManager(manager);
                    adapterFavSections = new AdapterFavSections(getContext(), arrayList);
                    recyclerView.setAdapter(adapterFavSections);


                    //dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //En caso de no poder acceder al servicio se muestra uno de los siguientes mensajes de error.

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getContext(), R.string.authFailureError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getContext(), R.string.serverError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getContext(), R.string.parseError, Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Se agrega la petición
        VolleySingleton.getIntanciaVolley(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onFavAllyClicked(FavAllyViewHolder holder, int position) {
        Toast.makeText(getContext(), "TEST", Toast.LENGTH_SHORT).show();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
