package com.mlaj.scard.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.snowdream.android.widget.SmartImageView;
import com.mlaj.scard.Adapters.AdapterAllyImg;
import com.mlaj.scard.Models.Ally;
import com.mlaj.scard.Models.VolleySingleton;
import com.mlaj.scard.R;
import com.mlaj.scard.Utils.Preferences;
import com.mlaj.scard.Utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;

//Fragmento donde se visualiza la informacion de una tienda especifica
public class AllyFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    //Inicializacion de paramentros que se reciben o envian como argumentos
    private static final String ARG_PARAM3 = "param3";
    @Nullable
    private Ally ally;

    JsonObjectRequest jsonObjectRequest;

    TextView tvAllyTitle;
    TextView tvAllyDesc;
    TextView tvAllyPromTitle;
    TextView tvAllyPromDesc;
    TextView tvAllyPromItem;
    TextView tvAllyPromItemDesc;
    TextView tvAllyNewsTitle;
    TextView tvAllyNewsDesc;
    TextView tvAllyNewsItem;
    TextView tvAllyNewsItemDesc;

    SmartImageView imgAlly;
    TextView tvAllyLocation;
    TextView tvAllyWebSite;
    TextView tvAllyContact;
    ArrayList<String> list;
    AdapterAllyImg adapterAllyImg;
    RecyclerView recyclerView;

    @Nullable
    private OnFragmentInteractionListener mListener;

    public AllyFragment() {
        // Required empty public constructor
    }

    //Metodo autogenerado y personalizado que se usa para la creacion de una nueva instancia.
    @NonNull
    public static AllyFragment newInstance(Ally ally) {
        AllyFragment fragment = new AllyFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM3, ally);
        fragment.setArguments(args);
        return fragment;
    }

    //Se obtienen los valores de los argumentos que fueron enviados al crear la nueva instancia.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ally = (Ally) getArguments().getSerializable(ARG_PARAM3);
        }
    }

    //Se infla el contenedor con el nuevo fragmento
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ally, container, false);

        //Cambio de titulo en la barra superior
        getActivity().setTitle(Utils.toTitleCase(ally.getName()));
        setHasOptionsMenu(true);
        //iniciacion de componentes de la vista
        tvAllyTitle = view.findViewById(R.id.tvAllyTitle);
        tvAllyDesc = view.findViewById(R.id.tvAllyDesc);
        tvAllyPromTitle = view.findViewById(R.id.tvAllyPromTitle);
        tvAllyPromDesc = view.findViewById(R.id.tvAllyPromDesc);
        tvAllyPromItem = view.findViewById(R.id.tvAllyPromItem);
        tvAllyPromItemDesc = view.findViewById(R.id.tvAllyPromItemDesc);
        tvAllyNewsTitle = view.findViewById(R.id.tvAllyNewsTitle);
        tvAllyNewsDesc = view.findViewById(R.id.tvAllyNewsDesc);
        tvAllyNewsItem = view.findViewById(R.id.tvAllyNewsItem);
        tvAllyNewsItemDesc = view.findViewById(R.id.tvAllyNewsItemDesc);

        imgAlly = view.findViewById(R.id.imgAlly);
        tvAllyLocation = view.findViewById(R.id.tvAllyLocation);
        tvAllyWebSite = view.findViewById(R.id.tvAllyWebSite);
        tvAllyContact = view.findViewById(R.id.tvAllyContact);

        //Se establecen valores a los componentes de la vista
        tvAllyTitle.setText(ally.getTitle());
        tvAllyDesc.setText(ally.getDesc());
        tvAllyPromTitle.setText(ally.getProm_item());
        tvAllyPromDesc.setText(ally.getProm_item_value());
        tvAllyPromItem.setText(ally.getProm_desc());
        tvAllyNewsTitle.setText(ally.getNews_title());
        tvAllyNewsDesc.setText(ally.getNews_desc());

        //se crea listener de para abrir la localizacion
        tvAllyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ally.getLocation() == null) {
                    Toast.makeText(getContext(), "No tiene una direccion actualmente", Toast.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent();
                    i.setAction(Intent.ACTION_VIEW);
                    //i.setData(Uri.parse("geo:5.0684277, -75.5136138"));
                    i.setData(Uri.parse("geo:0,0?q=" + ally.getLocation()));
                    Intent chooser = i.createChooser(i, "Lauch Maps");
                    startActivity(chooser);
                }
            }
        });

        //se crea listener de para abrir la pagina web
        tvAllyWebSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ally.getWebsite() != null) {
                    if (ally.getWebsite().equals("No Tiene")) {
                        Toast.makeText(getContext(), "No tiene un sitio web", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(ally.getWebsite()));
                        startActivity(i);
                    }
                } else {
                    Toast.makeText(getContext(), "No tiene un sitio web", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //se crea listener de para abrir la informacion de contacto
        tvAllyContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("ally", ally);
                InfoAllyFragment fragment = new InfoAllyFragment();
                fragment.setArguments(bundle);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_main, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        Rect rect = new Rect(imgAlly.getLeft(), imgAlly.getTop(), imgAlly.getRight(), imgAlly.getBottom());

        imgAlly.setImageUrl(ally.getLogo(), rect);

        list = new ArrayList<>();

        if (ally.getImage1().equals("null")){
        }else{
            if (ally.getImage1().equals("")){
            }else{
                list.add(Utils.URLIMG+ally.getImage1());
            }
        }

        if (ally.getImage2().equals("null")){
        }else{
            if (ally.getImage2().equals("")){
            }else{
                list.add(Utils.URLIMG+ally.getImage2());
            }
        }

        if (ally.getImage3().equals("null")){
        }else{
            if (ally.getImage3().equals("")){
            }else{
                list.add(Utils.URLIMG+ally.getImage3());
            }
        }

        if (ally.getImage4().equals("null")){
        }else{
            if (ally.getImage4().equals("")){
            }else{
                list.add(Utils.URLIMG+ally.getImage4());
            }
        }

        if (ally.getImage5().equals("null")){
        }else{
            if (ally.getImage5().equals("")){
            }else{
                list.add(Utils.URLIMG+ally.getImage5());
            }
        }

        //Se llena el recyclerview
        recyclerView = view.findViewById(R.id.rvAllyImg);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.setReverseLayout(false);
        recyclerView.setLayoutManager(layoutManager);
        adapterAllyImg = new AdapterAllyImg(list);

        recyclerView.setAdapter(adapterAllyImg);
        return view;
    }


    /**
     * Web Service que carga las secciones
     */

    private void cargarWebService(String state) {

        if (state.equals("")){
            state = "0";
        }

        //URL donde se encuentra alojado el Servicio.
        String url = Utils.URL + "/wsJSONFavUpdate.php?user_id_user="+
                Preferences.getPreferenceS(getContext(), Utils.ID_USER)+
                "&ally_id_ally="+ally.getId_ally()+
                "&state="+state+
                "&section="+ally.getSection_id_section();
        //inicialización de JSON donde se pide un objeto a traves de un GET
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Se obtiene la respuesta del servicio
                String json = response.optString("status");

                if (json.equals("add")){
                    Preferences.setPreferenceS(getContext(), Utils.LOADSECTION + ally.getSection_id_section(), "1");
                }else if (json.equals("delete")){
                    Preferences.setPreferenceS(getContext(), Utils.LOADSECTION + ally.getSection_id_section(), "1");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //En caso de no poder acceder al servicio se muestra uno de los siguientes mensajes de error.

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getContext(), R.string.authFailureError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getContext(), R.string.serverError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getContext(), R.string.parseError, Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Se agrega la petición
        VolleySingleton.getIntanciaVolley(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        String ally_fav = ally.getAlly_fav();
        if (ally_fav.equals("1")){
            inflater.inflate(R.menu.favorites_menu,menu); // TU MENU
            menu.findItem(R.id.btn_fav).setIcon(R.drawable.ic_star);
        }else{
            inflater.inflate(R.menu.favorites_menu,menu); // TU MENU
            menu.findItem(R.id.btn_fav).setIcon(R.drawable.ic_star_border);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.btn_fav:
                String user_id = Preferences.getPreferenceS(getContext(),Utils.ID_USER);
                if (Preferences.getPreferenceS(getContext(),Utils.FAV+ally.getId_ally()+"_"+user_id).equals("1")) {
                    cargarWebService(Preferences.getPreferenceS(getContext(),Utils.FAV+ally.getId_ally()+"_"+user_id));
                    Preferences.setPreferenceS(getContext(), Utils.FAV + ally.getId_ally()+"_"+user_id, "0");
                    item.setIcon(R.drawable.ic_star_border);
                    Toast.makeText(getContext(),"Se a borrado de favoritos", Toast.LENGTH_SHORT).show();
                }else{
                    cargarWebService(Preferences.getPreferenceS(getContext(),Utils.FAV+ally.getId_ally()+"_"+user_id));
                    Preferences.setPreferenceS(getContext(), Utils.FAV + ally.getId_ally()+"_"+user_id, "1");
                    item.setIcon(R.drawable.ic_star);
                    Toast.makeText(getContext(),"Se a agregado a favoritos", Toast.LENGTH_SHORT).show();
                }
        }
        return super.onOptionsItemSelected(item);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
