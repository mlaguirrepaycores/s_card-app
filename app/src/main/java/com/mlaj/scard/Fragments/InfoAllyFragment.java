package com.mlaj.scard.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.snowdream.android.widget.SmartImageView;
import com.mlaj.scard.Utils.Utils;
import com.mlaj.scard.Models.Ally;
import com.mlaj.scard.R;

//clase donde se visualiza la informacion de un aliado.
public class InfoAllyFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    LinearLayout linearFB;
    LinearLayout linearIG;
    LinearLayout linearEmail;
    LinearLayout linearPhone;

    SmartImageView ivPopLogo;

    TextView tvContactName;
    TextView tvContactFB;
    TextView tvContactIG;
    TextView tvContactEmail;
    TextView tvContactPhone;

    @Nullable
    Ally ally;

    public InfoAllyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_ally, container, false);
        //inicializacion de componentes de la vista
        Bundle bundle = getArguments();
        ally = (Ally) bundle.getSerializable("ally");
        getActivity().setTitle(Utils.toTitleCase(ally.getName()));
        //inicializacion de componentes
        linearFB = view.findViewById(R.id.linearFB);
        linearIG = view.findViewById(R.id.linearIG);
        linearEmail = view.findViewById(R.id.linearEmail);
        linearPhone = view.findViewById(R.id.linearPhone);
        ivPopLogo = view.findViewById(R.id.ivPopLogo);
        tvContactName = view.findViewById(R.id.tvContactName);
        tvContactFB = view.findViewById(R.id.tvContactFB);
        tvContactIG = view.findViewById(R.id.tvContactIG);
        tvContactEmail = view.findViewById(R.id.tvContactEmail);
        tvContactPhone = view.findViewById(R.id.tvContactPhone);

        //System.out.println(ally.toString());

        Rect rect = new Rect(ivPopLogo.getLeft(), ivPopLogo.getTop(), ivPopLogo.getRight(), ivPopLogo.getBottom());
        ivPopLogo.setImageUrl(ally.getLogo(), rect);

        String fb = "@" + ally.getFacebook();
        String ig = "@" + ally.getInstagram();
        tvContactName.setText(ally.getName());
        tvContactFB.setText(fb);
        tvContactIG.setText(ig);
        tvContactEmail.setText(ally.getEmail());
        tvContactPhone.setText(ally.getPhone());


        //validaciones de información,
        //si tienen información, se muestra , si no, se oculta
        if (ally.getFacebook() == null || ally.getFacebook().equals("null") || ally.getFacebook().equals("")) {
            linearFB.setVisibility(View.INVISIBLE);
        } else {
            linearFB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(getOpenFacebookIntent(getContext(), ally.getFacebook()));
                }
            });
        }

        if (ally.getInstagram() == null || ally.getInstagram().equals("null") || ally.getInstagram().equals("")) {
            linearIG.setVisibility(View.INVISIBLE);
        } else {
            linearIG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(getOpenInstagramIntent(getContext(), ally.getInstagram()));
                }
            });
        }

        if (ally.getEmail() == null || ally.getEmail().equals("null") || ally.getEmail().equals("")) {
            linearEmail.setVisibility(View.INVISIBLE);
        } else {
            linearEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(getOpenEmailIntent(getContext(), ally.getEmail()));
                }
            });
        }

        //System.out.println(ally.getPhone().equals("null"));
        if (ally.getPhone() == null || ally.getPhone().equals("null") || ally.getPhone().equals("")) {
            System.out.println(ally.getPhone() == null);

            linearPhone.setVisibility(View.INVISIBLE);

        }
        return view;
    }

    /**
     * verifica que tenga la aplicacion de FB y si la tiene abre el perfil desde la APP
     * en caso contrario abre el link en el navegador
     *
     * @param context
     * @return
     */
    public static Intent getOpenFacebookIntent(@NonNull Context context, String face) {

        String url1 = "https://m.facebook.com/" + face + "/posts";

        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);

            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://facewebmodal/f?href=" + url1));
        } catch (Exception e) {

            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/" + face));
        }
    }

    /**
     * verifica que tenga la aplicacion de Instagram y si la tiene abre el perfil desde la APP
     * en caso contrario abre el link en el navegador
     *
     * @param context
     * @return
     */
    public static Intent getOpenInstagramIntent(@NonNull Context context, String instagram) {
        try {
            context.getPackageManager().getPackageInfo("com.instagram.android", 0);

            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/_u/" + instagram));
        } catch (Exception e) {

            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/" + instagram));
        }
    }

    /**
     * Verifica que tenga una aplicacion para el envio de correos electronicos,
     * ademas solo muestra las opciones de estas.
     *
     * @param context
     * @return
     */
    public static Intent getOpenEmailIntent(Context context, String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setType("plain/text");
        intent.setData(Uri.parse("mailto:" + email));
        //intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "happytiendamanizales@gmail.com" });
        Intent chooser = Intent.createChooser(intent, "");
        return chooser;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
