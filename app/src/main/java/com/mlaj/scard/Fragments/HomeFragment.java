package com.mlaj.scard.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mlaj.scard.Adapters.AdapterSections;
import com.mlaj.scard.Adapters.SectionViewHolder;
import com.mlaj.scard.DetailsTransition;
import com.mlaj.scard.Interfaces.SectionClickListener;
import com.mlaj.scard.Models.Section;
import com.mlaj.scard.Models.VolleySingleton;
import com.mlaj.scard.MyProgressDialog;
import com.mlaj.scard.R;
import com.mlaj.scard.Utils.Preferences;
import com.mlaj.scard.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//Fragmento inicial donde se muestran las secctiones
public class HomeFragment extends Fragment implements SectionClickListener {

    ArrayList<Section> list;
    RecyclerView recyclerView;

    JsonObjectRequest jsonObjectRequest;
    JSONObject obj = null;
    MyProgressDialog dialog;

    @Nullable
    private OnFragmentInteractionListener mListener;

    String id_section, idImage, name, desc, allies;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //Se infla el contenedor con el nuevo fragmento
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inflado del fragmento en el contenedor del MAIN
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        //Cambio de titulo en la barra superior
        getActivity().setTitle(Utils.toTitleCase(getString(R.string.sections)));

        list = new ArrayList<>();

        //Se verifica la preferencia para saber si debe cargar el webService o no
        String load = Preferences.getPreferenceS(getContext(), Utils.LOADHOME);
        if (load.equals("0")) {
            sectionsWebService();
        } else {
            String object = Preferences.getJSON2(getContext(), Utils.WSHOME);
            try {
                obj = new JSONObject(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JSONArray json = obj.optJSONArray("client");

            try {
                for (int i = 0; i < json.length(); i++) {


                    //imagenes alojadas en el servidor se llaman a traves del url
                    String url = Utils.URLIMG + json.getJSONObject(i).getString("image");
                    url = url.replace(" ", "%20");

                    String secImage = url;
                    String secId = json.getJSONObject(i).getString("id_section");
                    String secName = json.getJSONObject(i).getString("name");
                    String secDesc = json.getJSONObject(i).getString("desc");
                    String secCount = json.getJSONObject(i).getString("allies");

                    Section section = new Section();
                    section.setId_section(secId);
                    section.setImage(secImage);
                    section.setName(secName);
                    section.setDesc(secDesc);
                    section.setAllies(secCount);
                    list.add(section);
                }

            } catch (JSONException e) {
                e.printStackTrace();

            }
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //se inicializa el listado
        //inicializacion del Layout manager en este caso Linear
        recyclerView = view.findViewById(R.id.rvSections);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);

        AdapterSections adapter = new AdapterSections(list, this);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Web Service que carga las secciones
     */

    private void sectionsWebService() {
        //Inicio de Progress Dialog

        dialog = new MyProgressDialog(getContext());
        dialog.setMessage("Comprobando ...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.show();

        //URL donde se encuentra alojado el Servicio.
        String url = Utils.URL + "/wsJSONSections.php";
        //inicialización de JSON donde se pide un objeto a traves de un GET
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Se obtiene la respuesta del servicio
                JSONArray json = response.optJSONArray("client");
                Preferences.setJSON2(getContext(), Utils.WSHOME, response.toString());

                try {

                    for (int i = 0; i < json.length(); i++) {
                        final Section section = new Section();

                        //imagenes alojadas en el servidor se llaman a traves del url
                        String url = Utils.URLIMG + json.getJSONObject(i).getString("image");
                        url = url.replace(" ", "%20");

                        //se obtienen los valores del json y luego son asignados al nuevo objeto
                        String secImage = url;
                        String secId = json.getJSONObject(i).getString("id_section");
                        String secName = json.getJSONObject(i).getString("name");
                        String secDesc = json.getJSONObject(i).getString("desc");
                        String secCount = json.getJSONObject(i).getString("allies");

                        section.setImage(secImage);
                        section.setId_section(secId);
                        section.setName(secName);
                        section.setDesc(secDesc);
                        section.setAllies(secCount);
                        list.add(section);

                        //Se cambia la preferencia para que no vuelva a cargar en lo que vive la aplicacion
                        Preferences.setPreferenceS(getContext(), Utils.LOADSECTION + secId, "1");
                    }

                    //Se crea el adaptador y se agrega el listener del click
                    final AdapterSections adapter = new AdapterSections(list, new SectionClickListener() {
                        @Override
                        public void onSectionClicked(SectionViewHolder holder, int position) {
                            id_section = list.get(position).getId_section();
                            idImage = list.get(position).getImage();
                            name = list.get(position).getName();
                            desc = list.get(position).getDesc();
                            allies = list.get(position).getAllies();

                            Section section = new Section();

                            section.setId_section(id_section);
                            section.setImage(idImage);
                            section.setName(name);
                            section.setDesc(desc);
                            section.setAllies(allies);

                            //Se usa el metodo newInstance del nuevo fragmento y se pasa como parametro la seccion.
                            SectionFragment fragment = SectionFragment.newInstance(section);

                            // Verifica que la api sea mayor a 21 y que tiene los permisos respectivos
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                fragment.setSharedElementEnterTransition(new DetailsTransition());
                                fragment.setEnterTransition(new Fade());
                                setExitTransition(new Fade());
                                fragment.setSharedElementReturnTransition(new DetailsTransition());
                            }

                            //Se inicia el siguiente fragmento y se usa el addSharedElement para crear el efecto de movimiento de la imagen
                            getActivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .addSharedElement(holder.imgSection, "example_transition")
                                    .replace(R.id.content_main, fragment)
                                    .addToBackStack(null)
                                    .commit();
                        }
                    });

                    //Se finaliza el progress dialog
                    dialog.dismiss();
                    recyclerView.setAdapter(adapter);
                    Preferences.setPreferenceS(getContext(), Utils.LOADHOME, "1");
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //En caso de no poder acceder al servicio se muestra uno de los siguientes mensajes de error.
                dialog.dismiss();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getContext(), R.string.authFailureError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getContext(), R.string.serverError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getContext(), R.string.parseError, Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Se agrega la petición
        VolleySingleton.getIntanciaVolley(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    //metodo implementado del SectionClickListener
    @Override
    public void onSectionClicked(@NonNull SectionViewHolder holder, int position) {
        id_section = list.get(position).getId_section();
        idImage = list.get(position).getImage();
        name = list.get(position).getName();
        desc = list.get(position).getDesc();
        allies = list.get(position).getAllies();

        Section section = new Section();

        section.setId_section(id_section);
        section.setImage(idImage);
        section.setName(name);
        section.setDesc(desc);
        section.setAllies(allies);

        SectionFragment fragment = SectionFragment.newInstance(section);

        // Verifica que la api sea mayor a 21 y que tiene los permisos respectivos
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setSharedElementEnterTransition(new DetailsTransition());
            fragment.setEnterTransition(new Fade());
            setExitTransition(new Fade());
            fragment.setSharedElementReturnTransition(new DetailsTransition());
        }

        //Se inicia el siguiente fragmento y se usa el addSharedElement para crear el efecto de movimiento de la imagen
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .addSharedElement(holder.imgSection, "example_transition")
                .replace(R.id.content_main, fragment)
                .addToBackStack(null)
                .commit();
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
