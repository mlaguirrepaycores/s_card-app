package com.mlaj.scard.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mlaj.scard.Adapters.AdapterSearch;
import com.mlaj.scard.Adapters.AllyViewHolder;
import com.mlaj.scard.DetailsTransition;
import com.mlaj.scard.Interfaces.AllyClickListener;
import com.mlaj.scard.Models.Ally;
import com.mlaj.scard.Models.VolleySingleton;
import com.mlaj.scard.MyProgressDialog;
import com.mlaj.scard.R;
import com.mlaj.scard.Utils.Preferences;
import com.mlaj.scard.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//clase que permite la busqueda de cualquier aliado.
public class SearchFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    AdapterSearch adapter;
    String section_id;
    JSONObject obj = null;
    ArrayList<Ally> list;
    RecyclerView recyclerView;
    GridLayoutManager manager;
    JsonObjectRequest jsonObjectRequest;
    MyProgressDialog dialog;
    String user_id;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        getActivity().setTitle(Utils.toTitleCase(getString(R.string.search)));
        setHasOptionsMenu(true);
        cargarWebService();
        list = new ArrayList<>();
        return view;
    }

    //metodo sobrecargado donde se llena el recyclerview
    //este metodo solo se usa cuando se quiere implementar el efecto de imagenes
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        manager = new GridLayoutManager(getContext(), 3);
        recyclerView = view.findViewById(R.id.rvGridAlly2);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);


        adapter = new AdapterSearch(list, new AllyClickListener() {

            //metodo implementado del AllyClickListener
            @Override
            public void onAllyClicked(@NonNull AllyViewHolder holder, int position) {
                AllyFragment fragment = AllyFragment.newInstance(list.get(position));

                // Verifica que la api sea mayor a 21 y que tiene los permisos respectivos
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    fragment.setSharedElementEnterTransition(new DetailsTransition());
                    fragment.setEnterTransition(new Fade());
                    setExitTransition(new Fade());
                    fragment.setSharedElementReturnTransition(new DetailsTransition());
                }

                //Se inicia el siguiente fragmento y se usa el addSharedElement para crear el efecto de movimiento de la imagen
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addSharedElement(holder.imgGrid, getString(R.string.exampleT))
                        .replace(R.id.content_main, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        recyclerView.setAdapter(adapter);
    }

    //Webservice que trae todos los aliados.
    private void cargarWebService() {
        //Inicio de Progress Dialog
        dialog = new MyProgressDialog(getContext());
        dialog.setMessage("Comprobando ...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.show();

        //URL donde se encuentra alojado el Servicio.

        String url = Utils.URL + "/wsJSONAllyAll.php";

        //inicialización de JSON donde se pide un objeto a traves de un GET
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Se obtiene la respuesta del servicio
                Preferences.setJSON2(getContext(),section_id, response.toString());

                String object = Preferences.getJSON2(getContext(), section_id);
                try {
                    obj = new JSONObject(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONArray json = obj.optJSONArray("client");
                try {
                    for (int i = 0; i < json.length(); i++) {
                        final Ally ally = new Ally();
                        String favorite;

                        String id_ally = json.getJSONObject(i).getString("id_ally");
                        String name = json.getJSONObject(i).getString("name");
                        String logo = json.getJSONObject(i).getString("logo");
                        String title = json.getJSONObject(i).getString("title");
                        String desc = json.getJSONObject(i).getString("descr");
                        String facebook = json.getJSONObject(i).getString("facebook");
                        String instagram = json.getJSONObject(i).getString("instagram");
                        String phone = json.getJSONObject(i).getString("phone");
                        String email = json.getJSONObject(i).getString("email");
                        String location = json.getJSONObject(i).getString("location");
                        String website = json.getJSONObject(i).getString("website");
                        String prom_item = json.getJSONObject(i).getString("prom_item");
                        String prom_item_value = json.getJSONObject(i).getString("prom_item_value");
                        String prom_desc = json.getJSONObject(i).getString("prom_desc");
                        String news_title = json.getJSONObject(i).getString("news_title");
                        String news_desc = json.getJSONObject(i).getString("news_desc");
                        String image1 = json.getJSONObject(i).getString("image1");
                        String image2 = json.getJSONObject(i).getString("image2");
                        String image3 = json.getJSONObject(i).getString("image3");
                        String image4 = json.getJSONObject(i).getString("image4");
                        String image5 = json.getJSONObject(i).getString("image5");
                        String section_id_section = json.getJSONObject(i).getString("section_id_section");
                        String ally_fav = json.getJSONObject(i).getString("user_id_user");
                        String iterator = i + "";

                        String url = Utils.URLIMG + "ally/" + logo;
                        url = url.replace(" ", "%20");

                        if (ally_fav.equals(user_id)) {
                            favorite = "1";
                        } else {
                            favorite = "0";
                        }


                        ally.setId_ally(id_ally);
                        ally.setName(name);
                        ally.setLogo(url);
                        ally.setTitle(title);
                        ally.setDesc(desc);
                        ally.setFacebook(facebook);
                        ally.setInstagram(instagram);
                        ally.setPhone(phone);
                        ally.setEmail(email);
                        ally.setLocation(location);
                        ally.setWebsite(website);
                        ally.setProm_item(prom_item);
                        ally.setProm_item_value(prom_item_value);
                        ally.setProm_desc(prom_desc);
                        ally.setNews_title(news_title);
                        ally.setNews_desc(news_desc);
                        ally.setImage1(image1);
                        ally.setImage2(image2);
                        ally.setImage3(image3);
                        ally.setImage4(image4);
                        ally.setImage5(image5);
                        ally.setSection_id_section(section_id_section);
                        ally.setAlly_fav(favorite);
                        ally.setIterator(iterator);
                        list.add(ally);
                    }

                    Preferences.setPreferenceS(getContext(), Utils.LOADSECTION + section_id, "0");
                    adapter = new AdapterSearch(list, new AllyClickListener() {

                        //metodo implementado del AllyClickListener
                        @Override
                        public void onAllyClicked(@NonNull AllyViewHolder holder, int position) {
                            AllyFragment fragment = AllyFragment.newInstance(list.get(position));

                            // Verifica que la api sea mayor a 21 y que tiene los permisos respectivos
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                fragment.setSharedElementEnterTransition(new DetailsTransition());
                                fragment.setEnterTransition(new Fade());
                                setExitTransition(new Fade());
                                fragment.setSharedElementReturnTransition(new DetailsTransition());
                            }

                            //Se inicia el siguiente fragmento y se usa el addSharedElement para crear el efecto de movimiento de la imagen
                            getActivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .addSharedElement(holder.imgGrid, getString(R.string.exampleT))
                                    .replace(R.id.content_main, fragment)
                                    .addToBackStack(null)
                                    .commit();
                        }
                    });

                    manager = new GridLayoutManager(getContext(), 3);
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setAdapter(adapter);

                    //Se finaliza el progress dialog
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }

                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //En caso de no poder acceder al servicio se muestra uno de los siguientes mensajes de error.
                dialog.dismiss();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getContext(), R.string.authFailureError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getContext(), R.string.serverError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), R.string.timeoutError, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getContext(), R.string.parseError, Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Se agrega la petición
        VolleySingleton.getIntanciaVolley(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint("Buscar");

        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                adapter.filter(searchQuery.toString().trim());
                return true;
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
