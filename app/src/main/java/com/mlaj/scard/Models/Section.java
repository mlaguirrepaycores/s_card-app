package com.mlaj.scard.Models;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import java.io.Serializable;

//Modelo de Secciones
public class Section implements Serializable{

    private String id_section;
    private String image;
    private String name;
    private String desc;
    private String allies;

    public Section() {
    }

    public String getId_section() {
        return id_section;
    }

    public void setId_section(String id_section) {
        this.id_section = id_section;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAllies() {
        return allies;
    }

    public void setAllies(String allies) {
        this.allies = allies;
    }

    @Override
    public String toString() {
        return "Section{" +
                "id_section='" + id_section + '\'' +
                ", image='" + image + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", allies='" + allies + '\'' +
                '}';
    }
}
