package com.mlaj.scard.Models;

import java.io.Serializable;

public class User implements Serializable {

    private String id_user;
    private String password;
    private String code;

    public User() {
    }

    public User(String id_user, String password, String code) {
        this.id_user = id_user;
        this.password = password;
        this.code = code;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "User{" +
                "id_user='" + id_user + '\'' +
                ", password='" + password + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
