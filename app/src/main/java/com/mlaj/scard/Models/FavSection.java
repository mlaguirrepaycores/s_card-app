package com.mlaj.scard.Models;

import java.util.ArrayList;

public class FavSection {

    String fav_title;
    ArrayList<Ally> list;

    public String getFav_title() {
        return fav_title;
    }

    public void setFav_title(String fav_title) {
        this.fav_title = fav_title;
    }

    public ArrayList<Ally> getList() {
        return list;
    }

    public void setList(ArrayList<Ally> list) {
        this.list = list;
    }
}
