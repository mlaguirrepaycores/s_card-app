package com.mlaj.scard.Models;

import java.io.Serializable;

//Modelo de aliados
public class Ally implements Serializable {

    private String id_ally;
    private String name;
    private String logo;
    private String title;
    private String desc;
    private String facebook;
    private String instagram;
    private String phone;
    private String email;
    private String location;
    private String website;
    private String prom_item;
    private String prom_item_value;
    private String prom_desc;
    private String news_title;
    private String news_desc;
    private String image1;
    private String image2;
    private String image3;
    private String image4;
    private String image5;
    private String section_id_section;
    private String ally_fav;
    private String iterator;

    public Ally() {
    }

    public String getId_ally() {
        return id_ally;
    }

    public void setId_ally(String id_ally) {
        this.id_ally = id_ally;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getProm_item() {
        return prom_item;
    }

    public void setProm_item(String prom_item) {
        this.prom_item = prom_item;
    }

    public String getProm_item_value() {
        return prom_item_value;
    }

    public void setProm_item_value(String prom_item_value) {
        this.prom_item_value = prom_item_value;
    }

    public String getProm_desc() {
        return prom_desc;
    }

    public void setProm_desc(String prom_desc) {
        this.prom_desc = prom_desc;
    }

    public String getNews_title() {
        return news_title;
    }

    public void setNews_title(String news_title) {
        this.news_title = news_title;
    }

    public String getNews_desc() {
        return news_desc;
    }

    public void setNews_desc(String news_desc) {
        this.news_desc = news_desc;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public String getSection_id_section() {
        return section_id_section;
    }

    public void setSection_id_section(String section_id_section) {
        this.section_id_section = section_id_section;
    }

    public String getAlly_fav() {
        return ally_fav;
    }

    public void setAlly_fav(String ally_fav) {
        this.ally_fav = ally_fav;
    }

    public String getIterator() {
        return iterator;
    }

    public void setIterator(String iterator) {
        this.iterator = iterator;
    }

    @Override
    public String toString() {
        return "Ally{" +
                "id_ally='" + id_ally + '\'' +
                ", name='" + name + '\'' +
                ", logo='" + logo + '\'' +
                ", title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", facebook='" + facebook + '\'' +
                ", instagram='" + instagram + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", location='" + location + '\'' +
                ", website='" + website + '\'' +
                ", prom_item='" + prom_item + '\'' +
                ", prom_item_value='" + prom_item_value + '\'' +
                ", prom_desc='" + prom_desc + '\'' +
                ", news_title='" + news_title + '\'' +
                ", news_desc='" + news_desc + '\'' +
                ", image1='" + image1 + '\'' +
                ", image2='" + image2 + '\'' +
                ", image3='" + image3 + '\'' +
                ", image4='" + image4 + '\'' +
                ", image5='" + image5 + '\'' +
                ", section_id_section='" + section_id_section + '\'' +
                ", ally_fav='" + ally_fav + '\'' +
                ", iterator='" + iterator + '\'' +
                '}';
    }
}
