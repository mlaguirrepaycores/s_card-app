package com.mlaj.scard.Interfaces;


import com.mlaj.scard.Fragments.AboutFragment;
import com.mlaj.scard.Fragments.AllyFragment;
import com.mlaj.scard.Fragments.CameraFragment;
import com.mlaj.scard.Fragments.FavoritesFragment;
import com.mlaj.scard.Fragments.HomeFragment;
import com.mlaj.scard.Fragments.InfoAllyFragment;
import com.mlaj.scard.Fragments.ProfileFragment;
import com.mlaj.scard.Fragments.SearchFragment;
import com.mlaj.scard.Fragments.SectionFragment;

public interface IFragments extends
        HomeFragment.OnFragmentInteractionListener,
        SectionFragment.OnFragmentInteractionListener,
        AllyFragment.OnFragmentInteractionListener,
        FavoritesFragment.OnFragmentInteractionListener,
        SearchFragment.OnFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener,
        CameraFragment.OnFragmentInteractionListener,
        AboutFragment.OnFragmentInteractionListener,
        InfoAllyFragment.OnFragmentInteractionListener{
}
