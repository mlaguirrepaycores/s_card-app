package com.mlaj.scard.Interfaces;

import com.mlaj.scard.Adapters.FavAllyViewHolder;

public interface FavAllyClickListener {

    void onFavAllyClicked(FavAllyViewHolder holder, int position);
}
