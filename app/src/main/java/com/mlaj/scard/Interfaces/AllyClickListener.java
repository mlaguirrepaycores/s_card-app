package com.mlaj.scard.Interfaces;

import com.mlaj.scard.Adapters.AllyViewHolder;

public interface AllyClickListener {

    void onAllyClicked(AllyViewHolder holder, int position);
}

