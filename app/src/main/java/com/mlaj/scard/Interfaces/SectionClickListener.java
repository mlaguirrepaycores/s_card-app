package com.mlaj.scard.Interfaces;

import com.mlaj.scard.Adapters.SectionViewHolder;

public interface SectionClickListener {

    void onSectionClicked(SectionViewHolder holder, int position);
}
